<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/login', function (Request $request) {
    $data = $request->validate([
        'email' => 'required|email',
        'password' => 'required'
    ]);

    $user = User::where('email', $request->email)->first();

    if (!$user || !Hash::check($request->password, $user->password)) {
        return response([
            'message' => ['These credentials do not match our records']
        ], 404);
    }

    $token = $user->createToken('tns-token')->plainTextToken;

    $response = [
        'user' => $user,
        'token' => $token
    ];

    return response($response, 201);
});
Route::post('/login', function (Request $request) {
    $request->validate([
        'name' => 'required|string',
        'email' => 'required|string|unique:users',
        'password' => 'required|string',
        'c_password' => 'required|same:password'
    ]);

    $user = new User([
        'name'  => $request->name,
        'email' => $request->email,
        'password' => bcrypt($request->password),
    ]);

    if ($user->save()) {
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->plainTextToken;

        return response()->json([
            'message' => 'Successfully created user!',
            'accessToken' => $token,
        ], 201);
    } else {
        return response()->json(['error' => 'Provide proper details']);
    }
});


//
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
